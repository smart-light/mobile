import React from 'react';
import {Container, Text, Button} from 'native-base';
import {AppStore} from '../store/AppStore';
import {observer} from 'mobx-react';

/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class Button
 */
type ButtonComponentProps = {
  appStore: AppStore;
};

@observer
export class ButtonComponent extends React.Component<ButtonComponentProps> {
  private readonly appStore: AppStore;

  constructor(props: ButtonComponentProps) {
    super(props);
    this.appStore = props.appStore;
  }

  testClick() {
    this.appStore.increment();
  }

  render() {
    return (
      <>
        <Container>
          <Button onPress={() => this.testClick()}>
            <Text>Button</Text>
          </Button>
        </Container>
      </>
    );
  }
}
