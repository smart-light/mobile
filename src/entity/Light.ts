/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class Light
 */
import {Expose} from 'class-transformer';
import { lightStore } from "../store/LightStore";

export class Light {
  public id!: string;
  public serialNumber!: string;
  @Expose({toClassOnly: true, name: 'isOn'})
  public enable!: boolean;


  public async off(): Promise<void> {
    this.enable = false;
    await lightStore.update(this);
  }
  public async on(): Promise<void> {
    this.enable = true;
    await lightStore.update(this);
  }
}
