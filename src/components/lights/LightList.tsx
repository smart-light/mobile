/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class LightList
 */
import React from 'react';
import {Light as LightEntity} from '../../entity/Light';
import {Light} from './Light';
import {Container, Content, List, ListItem, Body} from 'native-base';

type LightListProps = {
  lights: LightEntity[];
};

export class LightList extends React.Component<LightListProps> {
  public render() {
    if (!this.props.lights.length) {
      return null;
    }

    return (
      <>
        <Container>
          <Content>
            {this.props.lights.map((light, index) => {
              return <Light key={index} light={light} />;
            })}
          </Content>
        </Container>
      </>
    );
  }
}
