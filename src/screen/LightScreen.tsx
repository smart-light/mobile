/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class LightScreen
 */
import React from 'react';
import {lightStore, LightStore} from '../store/LightStore';
import {observer} from 'mobx-react';
import {Light} from '../components/lights/Light';
import { LightList } from "../components/lights/LightList";

type LightScreenProps = {
  lightStore: LightStore;
};

@observer
export class LightScreen extends React.Component<LightScreenProps> {
  private readonly lightStore: LightStore = lightStore;

  public async componentDidMount() {
    await this.lightStore.fetch();
  }

  public render() {
    if (!this.lightStore.lights.length) {
      return null;
    }

    return (
      <>
        <LightList lights={this.lightStore.lights} />
      </>
    );
  }
}
