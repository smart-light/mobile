/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class LightService
 */
import {Light} from '../entity/Light';
import {AxiosInstance} from 'axios';
import {plainToClass} from 'class-transformer';

export class LightService {
  constructor(private readonly axios: AxiosInstance) {}

  public async off() {
    throw new Error('Not implemented');
  }

  public async on() {
    throw new Error('Not implemented');
  }

  public async fetch(): Promise<Light[]> {
    const response = await this.axios.get('/api/v1/lights');
    return plainToClass<Light, any[]>(Light, response.data);
  }

  public async update(light: Light): Promise<Light> {
    const response = await this.axios.put('/api/v1/light', light);
    return plainToClass<Light, any>(Light, response.data);
  }
}
