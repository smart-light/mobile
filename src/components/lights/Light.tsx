/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class Light
 */
import React from 'react';
import {
  Button,
  Card,
  CardItem,
  Text,
  Body,
  Left,
  Right,
  Col,
  Container,
  Grid,
  Header,
  Row,
  Content,
} from 'native-base';
import {Light as LightEntity} from '../../entity/Light';
import {observer} from 'mobx-react';

type LightProps = {
  light: LightEntity;
};

@observer
export class Light extends React.Component<LightProps> {
  private async off(): Promise<void> {
    await this.props.light.off();
  }

  private async on(): Promise<void> {
    await this.props.light.on();
  }

  public render() {
    return (
      <Card>
        <CardItem header>
          <Text>Light {this.props.light.serialNumber}</Text>
        </CardItem>
        <CardItem bordered>
          <Body>
            <Text>
              Status: <Text>{this.props.light.enable ? 'On' : 'Off'}</Text>
            </Text>
          </Body>
        </CardItem>
        <CardItem footer bordered>
          <Button danger full onPress={() => this.off()}>
            <Text>Off</Text>
          </Button>

          <Button primary full onPress={() => this.on()}>
            <Text>On</Text>
          </Button>
        </CardItem>
      </Card>
    );
  }
}
