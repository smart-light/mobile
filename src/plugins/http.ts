/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class http
 */
import axios, {AxiosInstance} from 'axios';

export const http: AxiosInstance = axios.create({
  baseURL: 'https://smali.space',
  headers: {
    'Content-Type': 'application/json',
  },
});
