/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class LightStore
 */
import {Light} from '../entity/Light';
import {computed, observable} from 'mobx';
import {LightService} from '../services/LightService';
import {http} from '../plugins/http';

export class LightStore {
  constructor(private readonly lightService: LightService) {}

  @observable private _lights: Light[] = [];

  @computed get lights() {
    return this._lights;
  }

  public async fetch(): Promise<void> {
    this._lights = await this.lightService.fetch();
  }

  public async update(light: Light): Promise<void> {
    const updatedLight = await this.lightService.update(light);
    this._lights = this._lights.map((item) => {
      return item.id === updatedLight.id ? updatedLight : item;
    });
  }
}

export const lightStore = new LightStore(new LightService(http));
