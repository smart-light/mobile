/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class WelcomeScreen
 */
import React from 'react';
import {Text, Button, Col, Container, Grid, Row} from 'native-base';

export class WelcomeScreen extends React.Component {
  constructor(props: any) {
    super(props);
  }
  public render() {
    const {navigation} = this.props as any;

    return (
      <>
        <Container>
          <Grid>
            <Row>
              <Col>
                <Button onPress={() => navigation.navigate('Lights')}>
                  <Text>Lights</Text>
                </Button>
              </Col>
            </Row>
          </Grid>
        </Container>
      </>
    );
  }
}
