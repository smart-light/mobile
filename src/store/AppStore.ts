/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class AppStore
 */
import {computed, observable} from 'mobx';

export class AppStore {
  @observable private _count = 0;

  @computed get count() {
    return this._count;
  }

  increment() {
    this._count++;
  }
}

export const appStore = new AppStore();
